/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package com.urban.szkolenie.webservice;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.urban.szkolenie.webservice.model.Contact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * An endpoint class we are exposing
 */
@Api(name = "szkolenie", version = "v1", namespace = @ApiNamespace(ownerDomain = "webservice.szkolenie.urban.com", ownerName = "webservice.szkolenie.urban.com", packagePath = ""))
public class ContactEndpoint {

    static {
        ObjectifyService.register(Contact.class);
    }

    @ApiMethod(name = "list", httpMethod = "GET", path="/list")
    public List<Contact> listContacts() {
        return ofy().load().type(Contact.class).list();
    }

    @ApiMethod(name = "add", httpMethod = "POST", path="/add")
    public void addContact(Contact mContact) {
        ofy().save().entity(mContact).now();
    }

    @ApiMethod(name = "remove", httpMethod = "POST", path="/remove")
    public void removeContact(@Named("id") long id) {
        ofy().delete().type(Contact.class).id(id).now();
    }

}
